import java.sql.*;

public class Parent {
    static String dbName="reggie";
    static String connectionURL = "jdbc:derby:" + dbName + ";create=true";
    //static Connection conn = null;

    public static Connection getConn() throws  SQLException{
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(connectionURL);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }
    public static int executeSQL(String preparedSql, Object[] param){
        PreparedStatement pstmt = null;
        int num = 0;
        Connection conn = null;
        try{
            conn = getConn();
            pstmt = conn.prepareStatement(preparedSql);
            if(param != null){
                for(int i = 0; i < param.length; i++){
                    pstmt.setObject(i + 1, param[i]);
                }
            }

            num = pstmt.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            closeAll(conn, pstmt, null);
        }
        return num;
    }

    public static PreparedStatement setStatement (PreparedStatement pstmt, Object[] param) throws SQLException {
        if(param != null){
            for(int i = 0; i < param.length; i++){
                pstmt.setObject(i + 1, param[i]);
            }
        }
        return pstmt;
    }


    public static ResultSet select(String preparedSql, Object[] param){
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            conn = getConn();
            pstmt = conn.prepareStatement(preparedSql);
            pstmt = setStatement(pstmt, param);
            rs = pstmt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }
    public static void closeAll(Connection conn, PreparedStatement pstmt, ResultSet rs){
        if(rs != null){
            try {
                rs.close();
            } catch (SQLException e){
                e.printStackTrace();
            }
        }

        if(pstmt != null){
            try {
                pstmt.close();
            }
            catch (SQLException e){
                e.printStackTrace();
            }
        }

        if(conn != null){
            try {
                conn.close();
            } catch (SQLException e){
                e.printStackTrace();
            }
        }
    }
}
